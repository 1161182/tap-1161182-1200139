package domain

import domain.DomainTypes.{NonEmptyString, Quantity, TimeUnit}
import domain.DomainError.*
import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions

class DomainTypesTest extends AnyFunSuite:
  test("Invalid Quantity - negative value") {
    def value = -1

    def result = Quantity.from(value)

    def expected = Left(InvalidQuantity(value))

    assert(result == expected)
  }
  test("Zero Quantity") {
    def value = 0

    def result = Quantity.from(value)

    def expected = Left(InvalidQuantity(value))

    assert(result == expected)
  }
  test("Time Unit") {
    def value = 60

    def result = TimeUnit.from(value)

    def expected = Right(value)

    assert(result === expected)
    //alternative se não funcinasse implementar o to para o TimeUnit
    //assert(result.fold[Boolean](_=>false,v=>v.to == value))
  }
  test("Empty String") {
  def value = ""

  def result = NonEmptyString.from(value)

  def expected = Left(EmptyString)

  assert(result == expected)
}

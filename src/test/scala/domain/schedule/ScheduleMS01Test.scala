package domain.schedule

import scala.language.adhocExtensions
import scala.xml.Utility
import domain.DomainTypes.*
import domain.DomainError.*
import domain.Result

import io.FileIO.load
import io.FileIO.save
import org.scalatest.funsuite.AnyFunSuite


class ScheduleMS01Test extends AnyFunSuite :
  test("teste ms01 1 ") {
    val rb: Result[Boolean] =
      for
        xml <- load("files/assessment/ms01/validAgenda_01_in.xml")
        resultxml <- ScheduleMS01.create(xml)
        _ <- Right(save("files/teste1.xml", resultxml))
        expectedxml <- load("files/assessment/ms01/validAgenda_01_out.xml")
      yield Utility.trim(resultxml) == Utility.trim(expectedxml)
    assert(rb.fold(_ => false, b => b))
  }


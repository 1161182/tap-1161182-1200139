package domain.schedule

import scala.language.adhocExtensions
import scala.xml.Utility
import domain.DomainTypes.*
import domain.DomainError.*
import domain.Result

import io.FileIO.load
import io.FileIO.save
import org.scalatest.funsuite.AnyFunSuite


class ScheduleMS03Test  extends AnyFunSuite :
  test("teste rapido 1 ") {
    val rb: Result[Boolean] =
      for
        xml <- load("files/assessment/ms03/invalidHumanId_in.xml")
        resultxml <- ScheduleMS03.create(xml)
        _ <- Right(save("files/teste3.xml", resultxml))
        expectedxml <- load("files/assessment/ms03/invalidHumanId_outError.xml")
      yield Utility.trim(resultxml) == Utility.trim(expectedxml)
    assert(rb.fold(_ => false, b => b))
  }
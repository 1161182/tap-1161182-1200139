package assessment

import assessment.AssessmentTestMS02.{genHuman, genNonEmptyString, genOrderId, genPhysical, genProduct, genProductId, genQuantity, genTask}
import domain.DomainError.toXML
import domain.DomainTypes.{HumanId, NonEmptyString, OrderId, PhysicalId, PhysicalType, ProductId, Quantity, TaskId, TimeUnit}
import domain.order.{Order, Task}
import domain.product.Product
import domain.production.Production
import domain.production.Production.{humansForResources, manufacture, resourceAvaibled}
import domain.resource.{Human, Physical, PhysicalResource}
import domain.schedule.ScheduleMS01
import domain.schedule.ScheduleTask.listToXML
import domain.{DomainTypes, ResourceUnavaibledException}
import io.FileIO.save
import org.scalacheck.Gen.sequence
import org.scalacheck.Prop.forAll
import org.scalacheck.Test.Parameters
import org.scalacheck.{Gen, Properties, Test}

import java.util.UUID
import scala.language.adhocExtensions
import scala.util.{Failure, Success}

object AssessmentTestMS02 extends Properties("ProductionServiceProperties") :
  override def overrideParameters(prms: Test.Parameters): Test.Parameters =
    prms.withMinSuccessfulTests(300)

  val MAX_VALUE = 10

  def genNonEmptyString: Gen[NonEmptyString] =
    for
      n <- Gen.chooseNum(3, 5)
      listC <- Gen.listOfN(n, Gen.alphaChar)
      nestring <- NonEmptyString.from(listC.mkString).fold(_ => Gen.fail, nes => Gen.const(nes))
    yield nestring

  property("nonEmptyString") = forAll(genNonEmptyString)(
    nes => nes.toString != ("")
  )

  def genPhysicalId(s: String): Gen[PhysicalId] =
    for
      id <- PhysicalId.from("PRS_" + s).fold(_ => Gen.fail, prs => Gen.const(prs))
    yield id

  property("physicalId") = forAll(genPhysicalId("1"))(
    nes => nes.toString.startsWith("PRS_")
  )

  def genPhysicalType(s: String): Gen[PhysicalType] =
    for
      tp <- PhysicalType.from("PRST " + s).fold(_ => Gen.fail, prst => Gen.const(prst))
    yield tp

  property("physicalTypeStartsWithPRST_") = forAll(genPhysicalType("1"))(
    nes => nes.toString.startsWith("PRST ") && nes.toString.substring("PRST ".length).length > 0
  )

  property("physicalTypeStartsHasOnlyDigits") = forAll(genPhysicalType("1"))(
    nes => nes.toString.substring("PRST ".length) forall Character.isDigit
  )

  def genProductId(s: String): Gen[ProductId] =
    for
      id <- ProductId.from("PRD_"+s + genInt).fold(_ => Gen.fail, p => Gen.const(p))
    yield id

  property("productIdHasPrefixPRD_") = forAll(genProductId(""))(
    nes => nes.toString.startsWith("PRD_")
  )

  property("productIdHasOnlyDigits_") = forAll(genProductId(""))(
    nes => nes.toString.substring("PRD_ ".length) forall Character.isDigit
  )

  def genOrderId(s: String): Gen[OrderId] =
    for
      id <- OrderId.from("ORD_"+s + genInt).fold(_ => Gen.fail, p => Gen.const(p))
    yield id

  property("orderIdHasPrefixORD_") = forAll(genOrderId(""))(
    nes => nes.toString.startsWith("ORD_")
  )

  property("orderIdHasOnlyDigits_") = forAll(genOrderId(""))(
    nes => nes.toString.substring("ORD_ ".length) forall Character.isDigit
  )

  def genHumanId(s: String): Gen[HumanId] =
    for
      id <- HumanId.from("HRS_"+ s + genInt).fold(_ => Gen.fail, hrs => Gen.const(hrs))
    yield id

  property("humanIdHasPrefixHRS_") = forAll(genHumanId(""))(
    nes => nes.toString.startsWith("HRS_")
  )

  property("humanIdHasOnlyDigits_") = forAll(genHumanId(""))(
    nes => nes.toString.substring("HRS_ ".length) forall Character.isDigit
  )

  def genPhysical(s: String): Gen[Physical] =
    for
      id <- genPhysicalId(s + genInt)
      ptype <- genPhysicalType(s + genInt)
    yield Physical(id, ptype)

  def genHuman(lPhysical: List[Physical])(s: String): Gen[Human] =
    for
      id <- genHumanId(s)
      name <- genNonEmptyString
      lp <- genPhysicalList(lPhysical)
      lptype = lPhysical.map(f => f.atype)
    yield Human(id, name, lptype)

  def genInt: Int = (Math.random() * 100).toInt

  def genTaskId(s: String): Gen[TaskId] =
    for
      tid <- TaskId.from("TSK_"+s + genInt).fold(_ => Gen.fail, hrs => Gen.const(hrs))
    yield tid

  property("taskIdHasPrefixTSK_") = forAll(genTaskId(""))(
    nes => nes.toString.startsWith("TSK_")
  )

  property("taskIdHasOnlyDigits_") = forAll(genTaskId(""))(
    nes => nes.toString.substring("TSK_ ".length) forall Character.isDigit
  )

  def genTimeUnit: Gen[TimeUnit] =
    for
      tu <- TimeUnit.from(genInt).fold(_ => Gen.fail, t => Gen.const(t))
    yield tu

  property("timeUnitIsOnlyDigits") = forAll(genTimeUnit)(
    nes => nes.toString forall Character.isDigit
  )

  def genQuantity: Gen[Quantity] =
    for
      qt <- Quantity.from(genInt).fold(_ => Gen.fail, q => Gen.const(q))
    yield qt

  property("quantityIsOnlyDigits") = forAll(genQuantity)(
    nes => nes.toString forall Character.isDigit
  )

  def genTask(lPhysical: List[PhysicalType])(s: String): Gen[Task] =
    for
      id <- genTaskId(s)
      tu <- genTimeUnit
      lpt <- genPhysicalResourceList(lPhysical) //?? poderá usar repetidos
    yield Task(id, tu, lpt.map(p => PhysicalResource(p)))

  def genTasklist(lTask: List[Task]): Gen[List[Task]] =
    for
      n <- Gen.chooseNum(1, lTask.size)
      l <- Gen.pick(n, lTask)
    yield l.toList

  def genProduct(lTask: List[Task])(s: String): Gen[Product] =
    for
      id <- genProductId(s)
      name <- genNonEmptyString
      lt <- genTasklist(lTask)
    yield Product(id, name, lt)

  def genOrder(lProduct: List[Product])(s: String): Gen[Order] =
    for
      id <- genOrderId(s)
      p <- Gen.oneOf(lProduct)
      qt <- genQuantity
    yield Order(id, p, qt)

  def genPhysicalList(lPhysical: List[Physical]): Gen[List[Physical]] =
    for
      n <- Gen.chooseNum(1, lPhysical.size)
      l <- Gen.pick(n, lPhysical)
    yield l.toList

  def genPhysicalResourceList(lPhysical: List[PhysicalType]): Gen[List[PhysicalType]] =
    for
      n <- Gen.chooseNum(1, lPhysical.size)
      l <- Gen.pick(n, lPhysical)
    yield l.toList

  def genProduction: Gen[Production] =
    for
      n <- Gen.chooseNum(1, MAX_VALUE)

      listPhysicalId = (1 to n).map(i => "%02d".format(i)).toList
      listPhysicals = listPhysicalId.map(sid => genPhysical(sid))
      lPhysical <- Gen.sequence[List[Physical], Physical](listPhysicals)

      n1 <- Gen.chooseNum(1, MAX_VALUE)

      listHumanId = (1 to n).map( i => "%02d".format(i) ).toList
      listHumans = listHumanId.map( hid => genHuman(lPhysical)(hid) )
      lHuman <- Gen.sequence[List[Human], Human](listHumans)

      humanResource = lHuman.foldLeft[List[PhysicalType]](Nil)((_, h) => h.handles).toSet.toList
      physicalResource = lPhysical.filter(b => !humanResource.filter(hr => hr.equals(b.atype)).isEmpty)

      n2 <- Gen.chooseNum(1, MAX_VALUE)
      listTaskId = (1 to n2).map( i => "%02d".format(i) ).toList
      listTasks = listTaskId.map( tid => genTask(physicalResource.map(p=>p.atype))(tid) )
      lTask <- Gen.sequence[List[Task], Task](listTasks)

      n3 <- Gen.chooseNum(1, MAX_VALUE)
      listProductId = (1 to n3).map( i => "%02d".format(i) ).toList
      listProducts = listProductId.map( pid => genProduct(lTask)(pid) )
      lProduct <- Gen.sequence[List[Product], Product](listProducts)

      n4 <- Gen.chooseNum(1, MAX_VALUE)
      listOrderId = (1 to n4).map( i => "%02d".format(i) ).toList
      listOrders = listOrderId.map( oid =>  genOrder(lProduct)(oid) )
      lOrder <- Gen.sequence[List[Order], Order](listOrders) //Gen.listOfN(n, genOrder(lProduct))
    yield Production(lPhysical, lHuman, lTask, lProduct, lOrder)

  property("manufactureShouldReturnList") = forAll(genProduction)(
    (nes: Production) => {
      val uuid = UUID.randomUUID()
      manufacture(nes).fold(
        e => {
          save("files/pbt/error/manufacture." + uuid + ".xml", toXML(e))
          false
        },
        res => {
          save("files/pbt/success/manufacture." + uuid + ".xml", listToXML(res))
          !res.isEmpty
        }
      )
    }
  )
  property("The Same Resource Cannot Be Used At The Same Time By Two Tasks") = forAll(genProduction)(
    (prod: Production) => {
      try {
        prod.lTask.forall(task => humansForResources(task.id, resourceAvaibled(task,prod.lPhysical), prod.lHuman).size == task.resource.map(f => f.resource).size)
      } catch {
        case _: ResourceUnavaibledException => false
      }
    })

  property("Test Physical Resource") = forAll(genProduction)(
    (prod: Production) => {
      try {
        prod.lTask.forall(task => resourceAvaibled(task,prod.lPhysical).size == task.resource.map(b => b.resource).size)
      } catch {
        case _: ResourceUnavaibledException => false
      }
    }
  )

  /*property("The complete schedule must schedule all the tasks of all the products needed") = forAll(genProduction)(
    (production: Production) => {

      val uuid = UUID.randomUUID()
      manufacture(production).fold(
        e => {
          save("files/pbt/error/task." + uuid + ".xml", toXML(e))
          true
        },
        res => {
          val allOrderTasks = production.lOrder.flatMap((o: Order) => o.product.tasks.map(t => t.id))
          val allManufactureTasks = res.map(r => r.taskId)

          save("files/pbt/success/task." + uuid + ".xml", listToXML(res))
          allManufactureTasks.toSeq.toList.size == allOrderTasks.toSeq.toList.size
        }
      )
    }
  )*/

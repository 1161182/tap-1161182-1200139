package domain

import scala.xml.Elem

type Result[A] = Either[DomainError, A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case InvalidTaskId(error: String)
  case InvalidOrderId(error: String)
  case InvalidTime(error: Int)
  case InvalidQuantity(error: Int)
  case TaskUsesNonExistentPRT(error: String)
  case InvalidType(error: String)
  case ResourceNotExist(error: String)
  case InvalidPhysicalId(error: String)
  case InvalidProductId(error: String)
  case InvalidHumanId(error: String)
  case EmptyString
  case ResourceUnavailable(taskId: String, resourceId: String)
  case ProductDoesNotExist(error: String)
  case TaskDoesNotExist(error: String)
  case ImpossibleSchedule

object DomainError:
  def toXML(domainError: DomainError): Elem =
      <ScheduleError xmlns="http://www.dei.isep.ipp.pt/tap-2021"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../scheduleError.xsd "
                     message={domainError.toString}/>

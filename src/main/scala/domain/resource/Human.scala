package domain.resource

import domain.DomainTypes.{HumanId, NonEmptyString, PhysicalType}
import domain.Result
import domain.resource.PhysicalResource
import xml.XML.{fromAttribute, traverse}

import scala.xml.{Elem, Node}

final case class Human(id: HumanId, name: NonEmptyString, handles: List[PhysicalType])

object Human:
  def toXML(h: Human): Elem = <Human name={h.name.toString}/>

  def from(xml: Node): Result[Human] =
    for
      sid <- fromAttribute(xml, "id")
      id <- HumanId.from(sid)
      sname <- fromAttribute(xml, "name")
      name <- NonEmptyString.from(sname)
      resource <- traverse((xml \\ "Handles"), PhysicalResource.fromPT)
    yield Human(id, name, resource)

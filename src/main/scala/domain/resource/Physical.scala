package domain.resource

import domain.DomainError.TaskUsesNonExistentPRT
import domain.DomainTypes.{PhysicalId, PhysicalType}
import domain.Result
import domain.resource.Resource
import xml.XML.fromAttribute

import scala.xml.{Elem, Node}

final case class Physical(id: PhysicalId, atype: PhysicalType)

object Physical:
  def toXML(p: Physical): Elem = <Physical id={p.id.toString}/>

  def from(xml: Node): Result[Physical] =
    for
      sid <- fromAttribute(xml, "id")
      id <- PhysicalId.from(sid)
      stype <- fromAttribute(xml, "type")
      atype <- PhysicalType.from(stype)
    yield Physical(id, atype)

final case class PhysicalResource(resource: PhysicalType)

object PhysicalResource:
  def from(lphysical: List[Physical])(xml: Node): Result[PhysicalResource] =
    for
      stype <- fromAttribute(xml, "type")
      physicalType <- PhysicalType.from(stype)
      resource <- lphysical.find(p => p.atype == physicalType).fold(Left(TaskUsesNonExistentPRT(physicalType.to)))(p => Right(p.atype))
    yield PhysicalResource(resource)

  def fromPT(xml: Node): Result[PhysicalType] =
    for
      stype <- fromAttribute(xml, "type")
      pType <- PhysicalType.from(stype)
    yield pType
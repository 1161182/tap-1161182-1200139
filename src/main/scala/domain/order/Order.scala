package domain.order

import domain.DomainError.ProductDoesNotExist
import domain.DomainTypes.{OrderId, ProductId, Quantity}
import domain.Result
import domain.product.Product
import xml.XML.fromAttribute

import scala.xml.{Elem, Node}

final case class Order(id: OrderId, product: Product, quantity: Quantity)

object Order:
  def from(lprod: List[Product])(xml: Node): Result[Order] =
    for
      sid <- fromAttribute(xml, "id")
      id <- OrderId.from(sid)
      sprod <- fromAttribute(xml, "prdref")
      prdref <- ProductId.from(sprod)
      prodId <- lprod.find(p => p.id == prdref).fold(Left(ProductDoesNotExist(prdref.to)))(p => Right(p))
      sqntd <- fromAttribute(xml, "quantity")
      qntd <- Quantity.from(sqntd)
    yield Order(id, prodId, qntd)

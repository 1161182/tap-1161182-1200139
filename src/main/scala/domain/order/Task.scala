package domain.order

import domain.DomainTypes.{TaskId, TimeUnit}
import domain.DomainError.TaskDoesNotExist
import domain.Result
import domain.resource.{Physical, PhysicalResource}
import xml.XML.{fromAttribute, traverse}

import scala.xml.Node

final case class Task(id: TaskId, time: TimeUnit, resource: List[PhysicalResource])

object Task:
  def fromTask(lPhysical: List[Physical])(xml: Node): Result[Task] =
    for
      sid <- fromAttribute(xml, "id")
      id <- TaskId.from(sid)
      stime <- fromAttribute(xml, "time")
      time <- TimeUnit.from(stime)
      resource <- traverse((xml \\ "PhysicalResource"), PhysicalResource.from(lPhysical))
    yield Task(id, time, resource)

  def fromProcess(lTask: List[Task])(xml: Node): Result[Task] =
      for
        staskRef <- fromAttribute(xml, "tskref")
        taskId <- TaskId.from(staskRef)
        taskRef <- lTask.find(t => t.id == taskId).fold(Left(TaskDoesNotExist(taskId.to)))(t => Right(t))
      yield taskRef

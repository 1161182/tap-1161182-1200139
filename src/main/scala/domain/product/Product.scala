package domain.product

import domain.DomainTypes.{NonEmptyString, ProductId}
import domain.Result
import domain.order.Task
import xml.XML.{fromAttribute, traverse}

import scala.xml.Node

final case class Product(id: ProductId, name: NonEmptyString, tasks: List[Task])

object Product:
  def from(lTask: List[Task])(xml: Node): Result[Product] =
    for
      sid <- fromAttribute(xml, "id")
      id <- ProductId.from(sid)
      sname <- fromAttribute(xml, "name")
      name <- NonEmptyString.from(sname)
      tasks <- traverse((xml \\ "Process"), Task.fromProcess(lTask))
    yield Product(id, name, tasks)

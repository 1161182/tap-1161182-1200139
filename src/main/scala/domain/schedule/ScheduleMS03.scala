package domain.schedule

import domain.DomainError.toXML

import scala.xml.Elem
import domain.Result
import domain.production.Production.{from, manufactureMultiTask}
import domain.schedule.ScheduleTask.listToXML


object ScheduleMS03 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml element
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
 def create(xml: Elem): Result[Elem] =
  val taskSchedule = for
    factoryMultiTaskResult <- from(xml)
    res <- manufactureMultiTask(factoryMultiTaskResult)
  yield
    res

  val elem = (taskSchedule).fold(
    domainError => toXML(domainError),
    taskSchedules => listToXML(taskSchedules)
  )

  Right(elem)

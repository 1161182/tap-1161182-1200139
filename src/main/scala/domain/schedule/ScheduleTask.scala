package domain.schedule

import domain.DomainTypes.{OrderId, TaskId}
import domain.resource.{Human, Physical}

import scala.xml.Elem

case class ScheduleTask(
                         orderId: OrderId,
                         productNumber: String,
                         taskId: TaskId,
                         start: String,
                         end: String,
                         physicalResources: List[Physical],
                         humanResources: List[Human]
                       )

object ScheduleTask:
  def listToXML(taskSchedules: List[ScheduleTask]): Elem =
    <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
      {taskSchedules.map(ts => toXML(ts))}
    </Schedule>

  private def toXML(ts: ScheduleTask): Elem =
    <TaskSchedule order={ts.orderId.to} productNumber={ts.productNumber} task={ts.taskId.to}
                  start={ts.start} end={ts.end}>
      <PhysicalResources>
        {ts.physicalResources.map(pr => Physical.toXML(pr))}
      </PhysicalResources>
      <HumanResources>
        {ts.humanResources.map(hr => Human.toXML(hr))}
      </HumanResources>
    </TaskSchedule>

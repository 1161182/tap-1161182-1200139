package domain.schedule

import domain.Result

import scala.xml.Elem

trait Schedule:
  def create(xml: Elem): Result[Elem]
  
package domain.schedule

import domain.DomainError.toXML
import domain.Result
import domain.order.Order
import domain.production.Production.{from, manufacture}
import domain.schedule.ScheduleTask.listToXML
import xml.XML

import scala.xml.Elem

object ScheduleMS01 extends Schedule :

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml element
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    val taskSchedule = for
      factoryResult <- from(xml)
      res <- manufacture(factoryResult)
    yield
      res

    val elem = (taskSchedule).fold(
      domainError => toXML(domainError),
      taskSchedules => listToXML(taskSchedules)
    )

    Right(elem)

package domain

import domain.DomainError.ResourceUnavailable

import java.lang.RuntimeException

case class ResourceUnavaibledException(domainError: ResourceUnavailable) extends RuntimeException

object ResourceUnavaibledException:
  def getError(e: ResourceUnavaibledException): ResourceUnavailable = e.domainError

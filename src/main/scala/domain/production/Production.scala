package domain.production

import domain.order.{Order, Task}
import domain.product.Product
import domain.resource.{Human, Physical, PhysicalResource}
import domain.schedule.ScheduleTask
import domain.{DomainError, DomainTypes, ResourceUnavaibledException, Result}
import xml.XML.traverse

import scala.xml.Elem

final case class Production(lPhysical: List[Physical],
                            lHuman: List[Human],
                            lTask: List[Task],
                            lProduct: List[Product],
                            lOrder: List[Order])

object Production:
  def from(xml: Elem): Result[Production] =
    for
      lPhysical <- traverse((xml \\ "Physical"), Physical.from)
      lHuman <- traverse((xml \\ "Human"), Human.from)
      lTask <- traverse((xml \\ "Task"), Task.fromTask(lPhysical))
      lProduct <- traverse((xml \\ "Product"), Product.from(lTask))
      lOrder <- traverse((xml \\ "Order"), Order.from(lProduct))
    yield Production(lPhysical, lHuman, lTask, lProduct, lOrder)

  def manufactureMultiTask(factory: Production): Result[List[ScheduleTask]] =
    try
      val orders=allOrders(factory)
      //, factory.lHuman, factory.lPhysical

      Right(orders)
    catch
      case e: ResourceUnavaibledException => Left(DomainError.ImpossibleSchedule)

  def allOrders(st: List[ScheduleTask]): List[ScheduleTask] = {
    val l=fnfactory.lOrder.foldLeft(List[ScheduleTask]())((st, order) => manufactureOrders(st, order, factory, lPhysical, lHuman, 0))
  }

  //
  def manufactureOrders(st: List[ScheduleTask],
                       order: Order,
                       factory: Production, lPhysical: List[Physical], lHuman:List[Human] , t: Int): List[ScheduleTask] =
   (1 to order.quantity.to).foldLeft(st)(
      (st, q) => order.product.tasks.foldLeft(st)((st, task) => executeMultiTask(st, factory, lPhysical, lHuman, task, order.id, q, t)))
  //lista tarefas
  //lista que falta
  def executeMultiTask(st: List[ScheduleTask],
                  factory: Production, lPhysical: List[Physical], lHuman:List[Human],
                  task: Task,
                  orderId: DomainTypes.OrderId,
                  productNumber: Int, lastStEndTime: Int): List[ScheduleTask] =
    val taskId = task.id
    val physicals = resourceAvaibled(task, lPhysical)

    //verificar repeticao de recursos
    val humans = humansForResources1(taskId, physicals, lHuman)
    //val hs = humans.fold[List[Human]](_=>Nil,a=>a)

    //If the list is empty return 0, otherwise return last endtime
    //val lastStEndTime: Int = st.lastOption.fold(0)(scheduleTask => scheduleTask.end.toInt)

    val start: Int = lastStEndTime
    val end: Int = start + task.time.to

    st ::: List(ScheduleTask(
    orderId,
    productNumber.toString,
    taskId,
    start.toString,
    end.toString,
    physicals,
    humans
  ))

  def manufacture(factory: Production): Result[List[ScheduleTask]] =
    try
      Right(factory.lOrder.foldLeft(List[ScheduleTask]())((st, order) => manufactureOrder(st, order, factory)))
    catch
      case e: ResourceUnavaibledException => Left(ResourceUnavaibledException.getError(e))

  def manufactureOrder(st: List[ScheduleTask],
                       order: Order,
                       factory: Production): List[ScheduleTask] =
    (1 to order.quantity.to).foldLeft(st)(
      (st, q) => order.product.tasks.foldLeft(st)((st, task) => executeTask(st, factory, task, order.id, q))
    )

  def executeTask(st: List[ScheduleTask],
                  factory: Production,
                  task: Task,
                  orderId: DomainTypes.OrderId,
                  productNumber: Int): List[ScheduleTask] =
    val taskId = task.id

    val physicals = resourceAvaibled(task,factory.lPhysical)
    //verificar repeticao de recursos
    val humans = humansForResources(taskId, physicals, factory.lHuman)
    //val hs = humans.fold[List[Human]](_=>Nil,a=>a)

    //If the list is empty return 0, otherwise return last endtime
    val lastStEndTime: Int = st.lastOption.fold(0)(scheduleTask => scheduleTask.end.toInt)

    val start: Int = lastStEndTime
    val end: Int = start + task.time.to

    st ::: List(ScheduleTask(
      orderId,
      productNumber.toString,
      taskId,
      start.toString,
      end.toString,
      physicals,
      humans
    ))

  def resourceAvaibled(task: Task, lPhysical: List[Physical]): List[Physical] =
    val lp = task.resource.toList
    val (lpr, lltp) = lp.foldLeft(List[Physical](), List[DomainTypes.PhysicalType]()) {
      case ((lpt, ltp), tp) => {
        //limpar recursos escolhidos
        val lclean = lPhysical.filter(a1 => lpt.filter(b1 => a1.equals(b1)).isEmpty)
        val l = lclean.filter(h1 => h1.atype == tp.resource)
        if (l.isEmpty)
          (lpt, ltp.appended(tp.resource))
        else
          (lpt.appended(l.head), ltp) // se nao estiver na lista
      }
    }
    if (!lltp.isEmpty)
      val resourceIdString = lltp.head.toString
      throw new ResourceUnavaibledException(
        DomainError.ResourceUnavailable(task.id.to, resourceIdString)
      )
    else
      lpr

  def humansFor_Resources(id: DomainTypes.TaskId, resources: List[Physical], humans: List[Human]): List[Human] =
    val neededResources = resources.map(r => r.atype)
    val listR=neededResources.map(x=>(x,humans.filter(h => !h.handles.filter(rt => rt==x).isEmpty).map(a => a.id))).sortWith((p1,p2) => p1._2.size<p2._2.size)
    val nrr=listR.filter(b => b._2.size<1)
    if (nrr.size>0) throw new ResourceUnavaibledException(DomainError.ResourceUnavailable(id.to, nrr.head._1.toString))
    val list=fn(id,humans,listR)
    return neededResources.foldLeft(List[Human]())   {
        case ((lh), nr) => {
          val t=list.filter(l=> l._1.equals(nr))
          if(t.isEmpty)
            throw new ResourceUnavaibledException(DomainError.ResourceUnavailable(id.to, nr.toString))
          val rhumn=t.head._2
          lh.appended(rhumn.fold(_ => throw new ResourceUnavaibledException(DomainError.ResourceUnavailable(id.to, t.head._1.toString)), f=>f))
        }
      }


  def fn(id: DomainTypes.TaskId, lHuman: List[Human], list: List[(DomainTypes.PhysicalType, List[DomainTypes.HumanId])]):List[(DomainTypes.PhysicalType,Result[Human])]=
    if(list.isEmpty) return List[(DomainTypes.PhysicalType,Result[Human])]()
    if(list.size<1) return List[(DomainTypes.PhysicalType,Result[Human])]()
    val nrHmn=list.head
    val lh2=list.drop(1)
    if(nrHmn._2.isEmpty || nrHmn._2.size<1) return (nrHmn._1,Left(DomainError.ResourceUnavailable(id.to, nrHmn._1.toString)))::fn(id,lHuman,lh2)
    val hId=nrHmn._2.head
    val lh3=lh2.map(l => (l._1, l._2.filter(p => !p.equals(hId))))
    val h=lHuman.filter(h => h.id.equals(hId))
    if(h.isEmpty || h.size<1)
      (nrHmn._1,Left(DomainError.ResourceUnavailable(id.to, nrHmn._1.toString)))::fn(id,lHuman,lh3)
    (nrHmn._1,Right(h.head))::fn(id,lHuman,lh3)

def humansForResources(id: DomainTypes.TaskId, resources: List[Physical], humans: List[Human]): List[Human] =
  val neededResources = resources.map(r => r.atype)
  val (hn, llnr) = neededResources.foldLeft(List[Human](), List[DomainTypes.PhysicalType]()) {
    case ((rh, lnr), nr) => {
      val lclean = humans.filter(a1 => rh.filter(b1 => a1.equals(b1)).isEmpty)
      val lh = lclean.filter(h1 => !h1.handles.filter(pt => pt == nr).isEmpty)
      if (lh.isEmpty)
        (rh, lnr.appended(nr))
      else
        (rh.appended(lh.head), lnr) // se nao estiver na lista
    }
  }

  if (!llnr.isEmpty)
    val resourceIdString = llnr.head.toString

    throw new ResourceUnavaibledException(
      DomainError.ResourceUnavailable(id.to, resourceIdString)
    )
  else
    hn
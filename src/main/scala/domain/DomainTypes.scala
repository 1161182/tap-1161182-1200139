package domain

import domain.{DomainError, Result}

import scala.annotation.targetName
import scala.util.Try

object DomainTypes:
  opaque type TaskId = String
  opaque type ProductId = String

  extension (id: TaskId)
    @targetName("toTaskId")
    def to: String = id
  opaque type TimeUnit = Int
  opaque type PhysicalType = String

  extension (id: ProductId)
    @targetName("toProductId")
    def to: String = id
  opaque type PhysicalId = String
  opaque type NonEmptyString = String

  extension (atype: TimeUnit)
    @targetName("toTimeUnit")
    def to: Int = atype
  opaque type Quantity = Int
  opaque type OrderId = String

  extension (atype: PhysicalType)
    @targetName("toPhysicalType")
    def to: String = atype
  opaque type HumanId = String

  object TaskId:
    def from(s: String): Result[TaskId] =
      val rx = "TSK_[0-9]+".r
      if (rx.matches(s)) then Right(s) else Left(DomainError.InvalidTaskId(s))

  object ProductId:
    def from(s: String): Result[ProductId] =
      val rx = "PRD_[0-9]+".r
      if (rx.matches(s)) then Right(s) else Left(DomainError.InvalidProductId(s))

  object TimeUnit:
    def from(s: String): Result[TimeUnit] =
      Try(s.toInt).fold[Result[TimeUnit]](_ => Left(DomainError.InvalidTime(0)), t => from(t))

    def from(t: Int): Result[TimeUnit] = if (t > 0) then Right(t) else Left(DomainError.InvalidTime(t))

  object PhysicalType:
    def from(s: String): Result[PhysicalType] =
      val rx = "PRST [0-9]+".r
      if (rx.matches(s)) then Right(s) else Left(DomainError.InvalidType(s))

  object PhysicalId:
    def from(s: String): Result[PhysicalId] =
      val rx = "PRS_[0-9]+".r
      if (rx.matches(s)) then Right(s) else Left(DomainError.InvalidPhysicalId(s))

  extension (atype: Quantity)
    @targetName("toQuantity")
    def to: Int = atype

  object NonEmptyString:
    def from(s: String): Result[NonEmptyString] =
      if (s.isEmpty) then Left(DomainError.EmptyString) else Right(s)

  object Quantity:
    def from(s: String): Result[Quantity] =
      Try(s.toInt).fold[Result[Quantity]](_ => Left(DomainError.InvalidQuantity(0)), t => from(t))

    def from(t: Int): Result[Quantity] = if (t > 0) then Right(t) else Left(DomainError.InvalidQuantity(t))

  extension (id: OrderId)
    @targetName("toOrderId")
    def to: String = id

  object OrderId:
    def from(s: String): Result[OrderId] =
      val rx = "ORD_[0-9]+".r
      if (rx.matches(s)) then Right(s) else Left(DomainError.InvalidOrderId(s))

  object HumanId:
    def from(s: String): Result[HumanId] =
      val rx = "HRS_[0-9]+".r
      if (rx.matches(s)) then Right(s) else Left(DomainError.InvalidHumanId(s))

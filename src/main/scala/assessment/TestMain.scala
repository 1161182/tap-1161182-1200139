package assessment

import domain.schedule.ScheduleMS03
import domain.schedule.ScheduleMS01
import io.FileIO
import io.FileIO.{load, save}
import scala.xml.Utility
import domain.Result
import java.io.File
import scala.xml.{Utility, XML}

object TestMain:
  def main(args: Array[String]): Unit =
    val file = new File("files/assessment/ms03/validAgenda_simple_01_in.xml")//invalidHumanResourceUnavailable_in.xml
    val fli = XML.loadFile(file)
    for
      lala <- ScheduleMS03.create(fli)
    yield
      save("files/teste3.xml", lala)

    for
      lala <- ScheduleMS01.create(fli)
    yield
      save("files/teste1.xml", lala)

    val rb: Result[Boolean] =
    for
      expectedxml1 <- load("files/teste1.xml")
      expectedxml3 <- load("files/teste3.xml")
    yield Utility.trim(expectedxml1) == Utility.trim(expectedxml3)
    print(rb)

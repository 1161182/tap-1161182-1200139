package assessment

import domain.Result
import domain.schedule.{Schedule, ScheduleMS01, ScheduleMS03}

import scala.xml.Elem

object AssessmentMS01 extends Schedule :
  def create(xml: Elem): Result[Elem] = ScheduleMS01.create(xml)

object AssessmentMS03 extends Schedule:
  def create(xml: Elem): Result[Elem] = ScheduleMS03.create(xml)
